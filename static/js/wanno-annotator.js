// wanno-annotator - An annotator.js plugin for wanno.
// Maxime Woringer & Guilhem Doulcier, 2016, GPL3+
var ann, Annotator

// Attach annotator
jQuery(function ($) {      
    ann = $('main').annotator()
    ann.annotator('addPlugin', 'Wanno')
})

// Extend annotator
Annotator.Plugin.Wanno = function (element, options) {
    Annotator.Plugin.apply(this, arguments)
}
jQuery.extend(Annotator.Plugin.Wanno.prototype, new Annotator.Plugin(), {
    events: {},
	options: {
	},
	pluginInit: function () {
		this.annotator
		.subscribe('annotationCreated', function (annotation) {
		    console.info(annotation)
		})
		.subscribe('annotationUpdated', function (annotation) {
		    console.info(annotation)    
		})
		.subscribe('annotationDeleted', function (annotation) {
		    console.info(annotation)
		})	  
  },
  myCustomMethod: function () {
  }
})
