from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse
from wiki import parser
from wiki.models import Page
import re, json

def annotations(request, path):
    try:
        path = re.findall("(.*)\/annotations$", path)[0]
    except:
        return HttpResponse("The request is  likely to be misformatted")
    p = get_object_or_404(Page, name=path)
    
    if request.method == 'GET':
        ann = parser.parse(p.text)["annotations"]
        js = []
        for a in ann:
            js.append({"text":a["text"], "quote":"samplequote",
                       "ranges": [{"start":"/p[1]",
                                   "end":"/p[1]",
                                   "startOffset":a["start"],
                                   "endOffset":a["end"]}]
                   })
        jss = json.dumps(js)
        return HttpResponse(jss, content_type='application/json')
    elif request.method == 'POST':
        par = parser.parse(p.text)
        ann = par["annotations"]
        idx = [int(i["annot"]) for i in ann]
        if idx==[]:
            i = -1
        else:
            i = max(idx)
        a = json.loads(request.body)
        print a["ranges"]
        m1 = a["ranges"][0]["startOffset"]-2
        m2 = a["ranges"][0]["endOffset"]-2

        print par["deparse"]
        ipp = False
        for l in par["deparse"]:
            if l[0]>m1:
                m1 += l[1]+3
                ipp = True
                break
        if not ipp and idx != []:
            m1+=par["deparse"][-1][1]+3
            
        ipp = False
        for l in par["deparse"]:
            if l[0]>m2:
                m2 += l[1]+3#+(2+len(str(i+1)))
                ipp = True
                break
        if not ipp and idx != []:
            m2+=par["deparse"][-1][1]+3        

        p.text = p.text[:m2]+":{}]".format(i+1)+p.text[m2:]
        p.text = p.text[:m1]+"[{}:".format(i+1)+p.text[m1:]

        p.text += "\n[a{}:{}:a{}]".format(i+1, a["text"], i+1)
        p.save()
        
        response = HttpResponse(content="", status=303)
        response["Location"] = reverse("annotations:id", args=[path, i+1])
        return response
    
def index(request):
    t = "Annotations store."
    return HttpResponse(t, content_type='application/json')


def annotation(request, path, number):
    """Returns a single annotation"""
    p = get_object_or_404(Page, name=path)
    ann = parser.parse(p.text)['annotations']
    jss = {}
    for a in ann:
        jss[str(a['annot'])] = a
    if not jss.has_key(str(number)):
        return HttpResponse("Annotation not found")
    a = jss[str(number)]
    a = {"text":a["text"], "quote":"samplequote",
         "ranges": [{"start":"/p[1]",
                     "end":"/p[1]",
                     "startOffset":a["start"],
                     "endOffset":a["end"]}]}
    return HttpResponse(json.dumps(a), content_type='application/json')
    ## T
    t = """{
    "id": "39fc339cf058bd22176771b3e3187329",
    "uri":"http://localhost:8000/wiki/html/LoremIpsum",
    "annotator_schema_version": "v1.0",
    "created": "2011-05-24T18:52:08.036814",
    "updated": "2011-05-26T12:17:05.012544", 
    "ranges":[{"start":"/p[1]","startOffset":24,"end":"/p[1]","endOffset":34}],
    "quote":"c des donn",
    "text":"Monsieur Babou vous aime"
    }"""
    return HttpResponse(t)
# Pour memoire
t = """[
{
"id": "39fc339cf058bd22176771b3e3187329",
"uri":"http://localhost:8000/wiki/html/LoremIpsum",
"annotator_schema_version": "v1.0",
"created": "2011-05-24T18:52:08.036814",
"updated": "2011-05-26T12:17:05.012544", 
"ranges":[{"start":"/p[1]","startOffset":24,"end":"/p[1]","endOffset":34}],
"quote":"c des donn",
"text":"test"
},
{
"ranges":[{"start":"/p[1]","startOffset":62,"end":"/p[1]","endOffset":73}],
"quote":"que tout va toto",
"text":"essai"
}
]

"""
