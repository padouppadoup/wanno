from django.conf.urls import patterns, url, include

from annotations import handler

urlpatterns = patterns('',
    url(r'^p/(?P<path>.*)$', handler.annotations, name='index'),                       
    url(r'search', handler.annotations, name='search'),                       
    url(r'^id/(?P<path>.*)/(?P<number>\d)$', handler.annotation, name='id'),                       
    #url(r'.*', handler.index, name='index'),
    #url(r'^$', handler.index, name='index'),
    #url(r'^html/(?P<path>.*)$', views.showpage, name='showhtml'),
    #url(r'^create/(?P<path>.*)$', views.create, name='create'),
    #url(r'^edit/(?P<path>.*)$', views.edit, name='edit'),
)

