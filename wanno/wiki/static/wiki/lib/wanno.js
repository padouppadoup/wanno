/////////////////////////////////////////////////////
// The main js functions for `wanno`
// By the wanno team
// GPLv3+, May 2016
/////////////////////////////////////////////////////
var annotations = {} // Global var to store annotations
var annotationsId = 0 // identifier of the annotation
var debug = 0

jQuery(function ($) { // Execute after loading the page
    var content = $('#annotations').annotator();
    content.annotator('addPlugin', 'Wanno');  // Bind the wanno plugin
    
    //var csrftoken = $.cookie('csrftoken');
    //content.data('annotator:headers', {
    //'X-CSRFToken': csrftoken
    //});
    
    $('#submit').on('click', function() {
	var html = $('#annotatedtext').html()
	var toSend = JSON.stringify({'html' : html, 'ann' :annotations})
	console.log(toSend); // Get the annotated text
	jQuery.post(window.location.pathname, toSend, function(data) {
	    $('.alert').text(JSON.parse(data).text) // update the alert div
	    // change the color of the div (TODO)
	});
    });
});

updateDOM = function(annId) { // Select the class annotator-hl that have no ID
    $('.annotator-hl').filter(function(){return !$(this).attr('annId');
    }).attr('annId',annId)
}

// Create a annotator plugin in order to overload the methods (onEdit, onCreate, onDelete)
Annotator.Plugin.Wanno = function (element, options) {
    Annotator.Plugin.apply(this, arguments)
}
jQuery.extend(Annotator.Plugin.Wanno.prototype, new Annotator.Plugin(), {
    events: {},
	options: {
	},
	pluginInit: function () {
		this.annotator
		.subscribe('annotationCreated', function (annotation) {
		    annotations[annotationsId]=annotation.text // save the annotation text
		    updateDOM(annotationsId) // Assign an id to the newly created span(s)
		    annotationsId += 1
		})
		.subscribe('annotationUpdated', function (annotation) {
		    annotations[annotation.highlights[0].attributes.annid.value]=annotation.text // get the annotation value
		})
		.subscribe('annotationDeleted', function (annotation) {
		    debug=annotation
		    if (!debug.highlights[0].classList.contains('annotator-hl-temporary')) {
			annotations[annotation.highlights[0].attributes.annid.value]=null // get the annotation value
		    }
		    // Remove from the table
		})	  
	},
})
