#-*-coding:utf-8-*-
# wanno wanno wiki modules
# By the wanno team, 2014-2016
# GPLv3+

from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext, loader
from django.core.urlresolvers import reverse
from django.utils import timezone

import parser
import json

from wiki.models import Page
import gitmanager

def index(request):
    """Returns the list of pages"""
    t = "Here are the wiki pages.<br />"
    t += ', '.join([p.name for p in Page.objects.all()])
    return HttpResponse(t)

def showpage(request, path):
    """The main page processor. It renders the page when they get created"""
    if request.method == 'POST' and request.is_ajax(): # Handle the edition case
        # Should migrate code from the edit function here
        try:
            p = Page.objects.get(name=path)
        except Exception:
            return HttpResponse(json.dumps({'status': 'error', 'text' : 'The document {} is not found, strange'.format(path)}))

        # Get the data
        html = json.loads(request.body)["html"]
        annotations = json.loads(request.body)["ann"]
        coordinates = parser.extract_coordinates(html)
        # - match the annotations with the coordinates to create a usable annotator json
        
        # - pypandoc

        
        # - edit the file, save the HTML copy and annotations, and commit.
        gitmanager.write_to_file(p.path, content=None, html=html, annotations=annotations, url=p.repository)

        return HttpResponse(json.dumps({'status': 'ok', 'text' : 'The document has been saved, nice'}))
    else: # Display the HTML page
        try:
            p = Page.objects.get(name=path)
        except Exception:
            return render(request, 'wiki/html/notfound.html', {'name': path, 'path': path})


        template = loader.get_template('wiki/html/page.html')

        content = ""
        try:
            content = gitmanager.get_content(p.path, url=p.repository, commit_id=None)
        except Exception as ex:
            print(ex)
        if not len(content):
            try:
                content = gitmanager.get_content(p.path+".html", url=p.repository, commit_id=None)
            except Exception as ex:
                print(ex)
                content = "Problem"
        return render(request, 'wiki/html/page.html', {'name': p.name,
                                                       'text': parser.parse(content)['text'],
                                                       'path': path,
                                                       'pub_date' : p.pub_date})


def create(request, path, txt="No text has been added yet, edit the page to add stuff"):
    """Create a page in the database"""
    p = Page(name=path, text=txt, pub_date=timezone.now())
    p.save()
    return HttpResponseRedirect(reverse("wiki:showhtml", args=(path,)))

def edit(request, path):
    """Show a form to edit the page, and handle the edition"""
    p = get_object_or_404(Page, name=path)
    if request.POST.has_key('text'):
        # Edit the database
        p.text = request.POST["text"]
        p.save()
        return HttpResponseRedirect(reverse("wiki:showhtml", args=(path,)))
    else:
        return render(request, 'wiki/html/edit.html', {'name': p.name,
                                                       'path': path,
                                                       'text' : p.text,
                                                       'pub_date': p.pub_date})
