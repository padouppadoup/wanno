from django.conf.urls import patterns, url, include

from wiki import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^html/(?P<path>.*)$', views.showpage, name='showhtml'),
    url(r'^create/(?P<path>.*)$', views.create, name='create'),
    url(r'^edit/(?P<path>.*)$', views.edit, name='edit'),
)

