""" """
import os
import tempfile
import binascii
import codecs
import json 
from git import Repo, Commit
import hashlib

try:
    import pypandoc
    parser = pypandoc.convert
except ImportError:
    def parser(*args,**kargs):
        return " <br/> <br/>USE PYPANDOC FOR PARSING <br/> <br/> <code>"+"<br /> ".join(args)+"</code>"

FOLDER_NAME = 'test'
URL = "git@gitlab.com:geeklhem/testrepo.git"
FILE_NAME = 'testtext.org'

def orgmode_hotfix(text):
    text = text.replace(r"\begin{equation}", "$$")
    text = text.replace(r"\end{equation}", "$$")
    text = text.replace(r"\begin{equation*}", "$$")
    text = text.replace(r"\end{equation*}", "$$")
    text = text.replace(r"\begin{align*}", r"$$ \begin{aligned}")
    text = text.replace(r"\begin{align}", r"$$ \begin{aligned}")
    text = text.replace(r"\end{align*}", r"\end{aligned} $$")
    text = text.replace(r"\end{align}", r"\end{aligned} $$")
    return text

def critic2html(text):    
    text = text.replace("{--", "<del>")
    text = text.replace("--}", "</del>")
    text = text.replace("{++", "<add>")
    text = text.replace("++}", "</add>")
    text = text.replace("{>>", '<span class="comment">')
    text = text.replace("<<}", '</span>')
    text = text.replace("{==", "<mark>")
    text = text.replace("==}", "</mark>")
    text = text.replace("{==", "<mark>")
    text = text.replace("==}", "</mark>")
    text = text.replace("~>", '</ins><del class="sub">')
    text = text.replace("~~}", '</del>')
    text = text.replace("{~~", '<ins class="sub">')
    return text

def open_repo(url, name=None):
    if name is None:
        name = "wanno_"+hashlib.sha1(url).hexdigest()
    path = os.path.join(tempfile.gettempdir(), name)
    if not os.path.exists(path):
        repo = Repo.clone_from(url, path)
    else:
        repo = Repo(path)
    print("Repo open in {}".format(path))
    return path, url, repo

def update_git(url, name=None):
    _, _, repo = open_repo(url, name)
    return repo.remotes.origin.pull()

def push_git(url, name=None):
    _, _, repo = open_repo(url, name)
    return repo.remotes.origin.push()

def write_to_file(file, content, html, annotations, url):
    path, url, repo = open_repo(url)
    files = []
    if content is not None:
        codecs.open(os.path.join(path,file), mode="w", encoding='utf-8').write(content)
        files.append(file)
    if html is not None:
        codecs.open(os.path.join(path,file+".html"), mode="w", encoding='utf-8').write(html)
        files.append(file+".html")
    if annotations is not None:
        annotations = json.dumps(annotations)
        if annotations == "{}":
            codecs.open(os.path.join(path,file+".wanno"), mode="w",encoding='utf-8').write(annotations)
        files.append(file+".wanno")
        
    if len(files):
        index = repo.index
        index.add(files)
        index.commit("Edited {} from web".format(file))

def detail(commit_id, url):
    path, url, repo = open_repo(url)
    commit = Commit(repo, binascii.a2b_hex(commit_id))
    tree = commit.tree
    fmt = os.path.basename(FILE_NAME).split(".")[-1]
    content = critic2html(tree[FILE_NAME].data_stream.read())
    if fmt == "org":
        content = orgmode_hotfix(content)
    context = {"url": url,
               "commit_id":commit_id,
               "fmt":fmt,
               "filename":FILE_NAME,
               "file": parser(content,
                              'html5',
                              format=fmt,
                              extra_args=['--mathjax']),
               'path': path}
    return context
