#-*-coding:utf-8-*-
# the wanno team, GPLv3+
# Developping a simple regexp-based system to parse annotations from text
#+format of an annotation delimitation is as follow [<id>: annotation whatever:<id>]

import bs4
import re
from operator import itemgetter, attrgetter, methodcaller


t0 = """[start] coucou [end]"""
t1 = """I like [0:kittens:0]"""
t2 = """I like [0:multiple:0] [1:kittens:1]"""
t3 = """I like [0:[1:nested:1] kittens:0]"""
t4 = """[0:I:0] like [1:multiple, [2:nested:2], [3:overlapping:1] kittens:3]"""

# Functions
def cumsum(it):
    total = 0
    for x in it:
        yield total
        total += x[1]

def extract_annotations(html):
    """Take valid HTML as input and removes the annotation <span> tags"""
    return 'Not implemented'

def extract_coordinates(html):
    """Takes a valid HTML as input and returns the coordinates of each annotation"""
    s=bs4.BeautifulSoup(html, "lxml")
    out=[]
    for i in s.find_all(class_='annotator-hl'):
        j=i
        path=''
        while j.name != '[document]' and j.name != 'body': # Go up to the hierarchy
            path = "{}[{}]/{}".format(j.name, s.find_all(j.name).index(j), path)
            j=j.parent
        path = path[:-1]
        out.append({"id": i['annid'],
                    'distance': len(i.previous),
                    'longueur': len(i.text),
                    'path': path})
    return out

def parse(st):
    """Parse annotations indices"""
    # Check for annotations consistency
    op = re.findall("\[(\d)\:", st) # Extract the list of opening tags
    cl = re.findall("\:(\d)\]", st) # Extract the list of closing tags
    se = list(set(op) & set(cl)) # Compute the list of matched tags
    se = sorted([int(s) for s in se])
    if not len(se)==len(cl)==len(op):
        raise Exception("Unmatched comments... Bad")

    # Extract tuples of coordinates
    opP = [(m.start(), len(m.group(0)), int(m.group(1)), "op") for m in re.finditer("\[(\d)\:", st)]
    clP = [(m.start(), len(m.group(0)), int(m.group(1)), "cl") for m in re.finditer("\:(\d)\]", st)]
    P = opP+clP
    P = sorted(P, key=itemgetter(0))
    off = list(cumsum(P))
    pos = [P[i][0]-off[i]+2 for i in range(len(P))]

    # Clean the string
    pur = re.sub("\[(\d)\:", "", st)
    pur = re.sub("\:(\d)\]", "", pur)

    ## ==== Parse annotations contents
    pur = re.sub("\[a(\d)\:(.*)\:a\d\]", "", pur) # Remove annotation contents

    dd = {}
    for m in re.findall(r"\[a(\d)\:(.*?)\:a\d\]", st):
        dd[m[0]]=m[1]

    ## ==== Combine everything
    gr = sorted(zip(P, pos), key=lambda x: (x[0][2], x[0][3])) # Sort by group index
    annots = [{"text": dd[str(gr[2*i][0][2])],
               "annot": gr[2*i][0][2],
               "start":gr[2*i+1][1],
               "end":gr[2*i][1]}
              for i in se]

        
    return {"text":pur, "annotations":annots, "deparse":zip(pos,off)}
#for m in re.finditer("\:(\d)\]", t4):
#     print '%02d-%02d: %s' % (m.start(), m.end(), m.group(1))
