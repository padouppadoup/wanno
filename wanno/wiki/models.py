from django.db import models

# Create your models here.
class Page(models.Model):
    """A wiki page"""
    name = models.CharField(max_length=2000)
    path = models.CharField(max_length=2000)
    repository = models.CharField(max_length=2000)
    ## Should check here whether the name is valid...
    ## AND UNIQUE!
    text = models.TextField()
    pub_date = models.DateTimeField('date published')
    
