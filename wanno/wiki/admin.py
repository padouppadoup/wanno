from django.contrib import admin

# Register your models here.
from wiki.models import Page

admin.site.register(Page)
