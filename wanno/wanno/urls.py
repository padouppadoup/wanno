from django.conf.urls import patterns, include, url
from django.contrib import admin


from django.conf import settings ## DEBUG


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'wanno.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^wiki/', include('wiki.urls', namespace="wiki")),
    url(r'^git/', include('gitview.urls', namespace="git")),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^annotations/', include("annotations.urls", namespace="annotations")),
)


if settings.DEV_SERVER: ## DEBUG !!!
    urlpatterns += patterns('',
        (r'^site_media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_PATH}),
    )
