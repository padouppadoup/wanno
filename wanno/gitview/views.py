import os
import tempfile
import binascii

from django.shortcuts import render
from django.http import HttpResponse
from git import Repo, Commit
try:
    import pypandoc
    parser = pypandoc.convert
except ImportError:
    def parser(*args,**kargs):
        return " <br/> <br/>USE PYPANDOC FOR PARSING <br/> <br/> <code>"+"<br /> ".join(args)+"</code>"
FOLDER_NAME = 'test'
URL = "git@gitlab.com:geeklhem/testrepo.git"
FILE_NAME = 'testtext.org'

def critic2html(text):
    text = text.replace("{--", "<del>")
    text = text.replace("--}", "</del>")
    text = text.replace("{++", "<add>")
    text = text.replace("++}", "</add>")
    text = text.replace("{>>", '<span class="comment">')
    text = text.replace("<<}", '</span>')
    text = text.replace("{==", "<mark>")
    text = text.replace("==}", "</mark>")
    text = text.replace("{==", "<mark>")
    text = text.replace("==}", "</mark>")
    text = text.replace("~>", '</ins><del class="sub">')
    text = text.replace("~~}", '</del>')
    text = text.replace("{~~", '<ins class="sub">')
    return text

def open_repo(name=FOLDER_NAME, url=URL):
    path = os.path.join(tempfile.gettempdir(), name)
    if not os.path.exists(path):
        repo = Repo.clone_from(url, path)
    else:
        repo = Repo(path)

    return path, url, repo

def index(request):
    path, url, repo = open_repo()
    context = {"url": url,
               "commits":  repo.iter_commits(),
               'path': path}
    return render(request, 'gitview/index.html', context)

def update_git(request):
    _, _, repo = open_repo()
    a = repo.remotes.origin.pull()
    return HttpResponse(a)


def detail(request, commit_id):
    path, url, repo = open_repo()
    commit = Commit(repo, binascii.a2b_hex(commit_id))
    tree = commit.tree
    fmt = os.path.basename(FILE_NAME).split(".")[-1]
    context = {"url": url,
               "commit_id":commit_id,
               "fmt":fmt,
               "filename":FILE_NAME,
               "file":parser(critic2html(tree[FILE_NAME].data_stream.read()), 'html', format=fmt),
               'path': path}
    return render(request, 'gitview/commit.html', context)
