from django.conf.urls import patterns, url, include

from gitview import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^commit/(?P<commit_id>[a-z0-9]+)$', views.detail, name='detail'),
    url(r'^update$', views.update_git, name='update'),
)
